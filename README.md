# jenkins-ansible-installation

Install and provision a Jenkins instance on CentOS 7 

# Usage

1. Install ansible on your local machine
2. Clone the repository: `git clone https://gitlab.com/peterjuma/jenkins-ansible-installation.git`
3. Change the working directory to the project directory: `cd jenkins-ansible-installation`
4. Change the "hosts" value in `jenkinsinstall.yml` to your Jenkins server
5. Run the ansible playbook `ansible-playbook jenkinsinstall.yml`
6. Access Jenkins on http://<jenkinshostip>:8080
7. Get the `initialAdminPassword` from the file "JenkinsArtifacts.txt" and proceed with Jenkins configuration.
   - Note: The file "JenkinsArtifacts.txt" will be created after the ansible playbook is run.

# The Ansible playbook will perform the following:

1.  Download Jenkins CentOS 7 repository and key
2.  Installation pre-requisite packages
        - java-1.8.0-openjdk
        - jenkins
        - git
3.  Start and enable the Jenkins service
4.  Configure the firewall to allow port 8080
5.  Read and output the initial Admin Password on the terminal
6.  Configure `jenkins` user to `/etc/sudoers.d/jenkins` 
7.  Allow jenkins user login 
8.  Generate ssh keypair for jenkins user 
9.  Copy both initial Admin Password, `jenkins` user Private Key and Public Key to a local file `JenkinsArtifacts.txt`